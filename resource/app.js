"use strict";

if (!/#Project\-.*/.test(new URL(window.location).hash)) return;

if ($("#ItemForm")) {
  $("div.FormField").each(function() {
    if ($(this).data("bind")) {
      let label;
      if (
        $(this)
          .find("div.FieldLabel")
          .css("height") == "5px"
      ) {
        label = $(this).find("label.SetLabel");
      } else {
        label = $(this).find("div.FieldLabel");
      }

      let fieldName = $(this)
        .data("bind")
        .match(/data\['(?<name>(\S*))']/).groups.name;

      var span = $("<span>", {
        css: {
          float: "right",
          color: "red"
        }
      });
      span.text(fieldName);

      label.append(span);
    }
  });
}
