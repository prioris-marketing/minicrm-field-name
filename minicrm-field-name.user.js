// ==UserScript==
// @name           MiniCRM - Field Name
// @description    ES6 script
// @author         Schronk Tamás <schronk.tamas@prioris.hu> (schronk.tamas@prioris.hu)
// @namespace      https://prioris.hu
// @version        1.0.0
// @icon           http://tampermonkey.net/favicon.ico
// @match          https://r3.minicrm.hu/*
// @grant          none
// @run-at         document-end
// @require        https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.js
// ==/UserScript==

/* jshint asi: true, esnext: true, -W097 */

var mfnf = function($, undefined) {
  $(function() {
    if (!/#Project\-.*/.test(new URL(window.location).hash)) return;

    if ($("#ItemForm")) {
      $("div.FormField").each(function() {
        if ($(this).data("bind")) {
          let label;
          if (
            $(this)
              .find("div.FieldLabel")
              .css("height") == "5px"
          ) {
            label = $(this).find("label.SetLabel");
          } else {
            label = $(this).find("div.FieldLabel");
          }

          let fieldName = $(this)
            .data("bind")
            .match(/data\['(?<name>(\S*))']/).groups.name;

          var span = $("<span>", {
            css: {
              float: "right",
              color: "red"
            }
          });
          span.text(fieldName);

          label.append(span);
        }
      });
    }
  });
};

setTimeout(function() {
  mfnf(window.jQuery.noConflict(true));
}, 2000);
